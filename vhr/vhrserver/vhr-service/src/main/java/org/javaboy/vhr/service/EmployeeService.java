package org.javaboy.vhr.service;

import org.javaboy.vhr.bean.Employee;
import org.javaboy.vhr.bean.MailConstants;
import org.javaboy.vhr.bean.MailSendLog;
import org.javaboy.vhr.bean.RespPageBean;
import org.javaboy.vhr.mapper.EmployeeMapper;
import org.javaboy.vhr.mapper.MailSendLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Author: 红颜祸水nvn <bai211425401@126.com>
 * @Description: CSDN <https://blog.csdn.net/qq_43647359>
 */
@Service
public class EmployeeService {
    @Autowired
    EmployeeMapper employeeMapper;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    MailSendLogService mailSendLogService;

    private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);
    SimpleDateFormat yearformat = new SimpleDateFormat("yyyy");
    SimpleDateFormat monthformat = new SimpleDateFormat("MM");
    DecimalFormat decimalFormat = new DecimalFormat("##.00");

    public RespPageBean getEmployeeByPage(Integer page, Integer size, Employee employee, Date[] beginDateScope) {
        if (page != null && size != null) {
            page = (page - 1) * size; // 分页公式
        }
        // 分页数据
        List<Employee> data = employeeMapper.getEmployeeByPage(page, size, employee, beginDateScope);
        // 数据总数量
        Long total = employeeMapper.getTotal(employee, beginDateScope);
        return new RespPageBean(total, data);
    }

    public Integer addEmployee(Employee employee) {
        // 添加员工时并计算合同期限
        Date endContract = employee.getEndContract();
        Date beginContract = employee.getBeginContract();
        double month = (Double.parseDouble(yearformat.format(endContract)) - Double.parseDouble(yearformat.format(beginContract))) * 12 + (Double.parseDouble(monthformat.format(endContract)) - Double.parseDouble(monthformat.format(beginContract)));
        employee.setContractTerm(Double.parseDouble(decimalFormat.format(month / 12)));
        int result = employeeMapper.insertSelective(employee);
        if (result == 1) {
            // 添加成功后给员工发送邮件
            Employee emp = employeeMapper.getEmployeeById(employee.getId());
            // 生成消息的唯一 id
            String msgId = UUID.randomUUID().toString();
            MailSendLog mailSendLog = new MailSendLog();
            mailSendLog.setMsgId(msgId);
            mailSendLog.setCreateTime(new Date());
            mailSendLog.setExchange(MailConstants.MAIL_EXCHANGE_NAME);
            mailSendLog.setRouteKey(MailConstants.MAIL_ROUTING_KEY_NAME);
            mailSendLog.setEmpId(emp.getId());
            mailSendLog.setTryTime(new Date(System.currentTimeMillis() + 1000 * 60 * MailConstants.MSG_TIMEOUT));
            mailSendLogService.insert(mailSendLog);
            rabbitTemplate.convertAndSend(MailConstants.MAIL_EXCHANGE_NAME, MailConstants.MAIL_ROUTING_KEY_NAME, emp, new CorrelationData(msgId));
            rabbitTemplate.convertAndSend(MailConstants.MAIL_EXCHANGE_NAME, MailConstants.MAIL_ROUTING_KEY_NAME, emp, new CorrelationData(msgId));
        }
        return result;
    }

    public Integer maxWorkID() {
        return employeeMapper.maxWorkID();
    }

    public Integer deleteEmployee(Integer id) {
        return employeeMapper.deleteByPrimaryKey(id);
    }

    public Integer updateEmployee(Employee employee) {
        // 修改员工时并计算合同期限
        Date endContract = employee.getEndContract();
        Date beginContract = employee.getBeginContract();
        double month = (Double.parseDouble(yearformat.format(endContract)) - Double.parseDouble(yearformat.format(beginContract))) * 12 + (Double.parseDouble(monthformat.format(endContract)) - Double.parseDouble(monthformat.format(beginContract)));
        employee.setContractTerm(Double.parseDouble(decimalFormat.format(month / 12)));
        return employeeMapper.updateByPrimaryKeySelective(employee);
    }

    public Integer addEmp(List<Employee> list) {
        return employeeMapper.addEmp(list);
    }

    public RespPageBean getEmployeeByPageWithSalary(Integer page, Integer size) {
        if (page != null && size != null) {
            page = (page - 1) * size;
        }
        List<Employee> employeeList = employeeMapper.getEmployeeByPageWithSalary(page, size);
        Long total = employeeMapper.getTotal(null, null);
        return new RespPageBean(total, employeeList);
    }

    public Integer updateEmpSalary(Integer eid, Integer sid) {
        return employeeMapper.updateEmpSalary(eid, sid);
    }

    public Employee getEmployeeById(Integer empId) {
        return employeeMapper.getEmployeeById(empId);
    }
}
