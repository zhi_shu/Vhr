package org.javaboy.vhr.bean;

import java.util.Date;

/**
 * 消息内容表
 */
public class MsgContent {
    /**
     * 编号
     */
    private Integer id;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 创建日期
     */
    private Date createdate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}