#### 项目介绍
Vhr 项目中文名称：微人事
此项目是由 Spring Boot + Vue 前后端分离技术开发，共同学习前后端分离开发项目，共同学习

#### 所用技术栈
## 后端技术栈				

1. Spring Boot				
2. Spring Security			
3. MyBatis				
4. MySQL				
5. Redis				
6. RabbitMQ				
7. Spring Cache				
8. WebSocket

## 前端所用技术栈
1. Vue
2. ElementUI	
3. axios
4. vue-router
5. Vuex
6. WebSocket
7. vue-cli4

#### 快速部署

1. clone 项目到本地 `git@gitee.com:jian_bo_bai/Vhr.git`
2. 导入 vhr.sql 脚本文件
3. 在 IntelliJ IDEA 中打开 vhr 目录，将 resources 下的 application.properties 数据库配置修改成自己本地的
4. 由于是前后端分离项目所以也要启动 vuehr 项目
5. 在 IntelliJ IDEA 中再次打开 vuehr 目录 （这里面你也可以使用 webStorm 开发，我这里习惯两个都是用 IDEA 开发，看你自己喜欢使用什么开发工具）
6. 进入到 vuehr 目录中，在命令行依次输入如下命令：

```
# 安装依赖
npm install
# 在 localhost:8080 上启动项目
npm run serve
```

由于我在 vuehr 项目中已经配置了端口转发，将数据转发到 Spring Boot 上，因此项目启动之后，在浏览器中输入 `http://localhost:8080` 就可以访问我们的前端项目了，所有的请求通过端口转发将数据传到 Spring Boot 中（注意此时不要关闭 Spring Boot 项目）。

7. 最后可以继续开发 vuehr 项目，开发完成后，当项目要上线时，依然进入到 vuehr 目录，然后执行如下命令：

```
# 将 vue 项目打包
npm run build
```

该命令执行成功之后，vuehr 目录下生成一个 dist 文件夹，将该文件夹中的两个文件 static 和 index.html 拷贝到 Spring Boot 项目中 resources/static/ 目录下，然后就可以像第 6 步那样直接访问了。

# 最后
如果大家学习过程中遇到什么问题的话，可以加我 QQ:211425401 ，备注：码云项目微人事学习
记得关注我的博客：https://blog.csdn.net/qq_43647359 谢谢大家了。




### 2020/4/5 新增内容
这次更新了后端接口，将服务端改为了聚合工程，其中添加了一个新的功能就是在添加员工的过程中给员工qq邮箱发送入职欢迎邮件
由于这次改动中加入了 RabbitMQ 消息队列，所以下次再要启动项目要注意以下几点：
1. 首先你的电脑要安装了 RabbitMQ ，无论是 windows 版本或者是 Linus 版本都可以，对应的只需要将其中的 properties 配置文件修改成你本机的配置即可。
2. 然后启动项目需要主要首先启动 vhr\mailserver 这个消息服务，然后在启动 vhrserver 这个服务，要注意先后顺序。
3. 这次添加了新的功能 -> 薪资管理 -> 工资账套管理模块 （CURD功能已实现）


### 2020/4/8 新增内容
1. 新增了薪资管理模块 > 员工账套设置 (实现了分页功能，修改功能)
2. 新增了在线聊天功能，登陆进去点击用户头像左边那个小喇叭即可进入在线聊天页面
	在线聊天需要你同时打开两个不同的浏览器，或者谷歌使用两个登陆用户来分别登陆
	这样就可以体验出在线聊天的功能。

### 2020/4/8 晚上新增内容
1. 新增了验证码验证登陆功能


### 2020/4/9 新增内容
2. 新增了用户个人信息的修改
3. 新增了用户个人密码的修改

### 2020/4/11 更新内容
这次修复了 RabbitMQ 的高可用性能，在使用 RabbitMQ消费消息失败的各种失败情况给予修复


### 2020/4/21 更新内容
添加了用户头像更新操作，使用FastDFS功能实现
要想体验此功能，首先要在自己的虚拟机上装一下必须的软件，详细链接：https://blog.csdn.net/qq_43647359/article/details/105645615

